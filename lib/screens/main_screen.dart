import 'package:flutter/material.dart';
import 'package:http_practice/services/unsplash_service.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Http demo'),
      ),
      body: FutureBuilder(
        future: UnsplashService.fetchDataToDto(),
        builder: (context, snapshot) => ListView.builder(
          itemCount: snapshot.data.length,
          itemBuilder: (ctx, index) => Card(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text(snapshot.data[index].id),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Image.network(snapshot.data[index].imageUrl['small']),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
