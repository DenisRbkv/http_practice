import 'package:flutter/material.dart';

class ImageDto {
  final String id;
  final String color;
  final String description;
  final Map<String, dynamic> imageUrl;

  ImageDto({
    @required this.id,
    @required this.color,
    @required this.description,
    @required this.imageUrl,
  });
}
