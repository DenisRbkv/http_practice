import 'package:http_practice/dto/image_dto.dart';
import 'package:http_practice/services/unsplash_service.dart';

class UnsplashRepository {
  static Future<List<ImageDto>> getData() async {
    return UnsplashService.fetchDataToDto();
  }
}
